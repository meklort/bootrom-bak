################################################################################
###
### @file       libbase/CMakeLists.txt
###
### @project
###
### @brief      libbase CMake file
###
################################################################################
###
################################################################################
###
### @copyright Copyright (c) 2018-2021, Evan Lojewski
### @cond
###
### All rights reserved.
###
### Redistribution and use in source and binary forms, with or without
### modification, are permitted provided that the following conditions are met:
### 1. Redistributions of source code must retain the above copyright notice,
### this list of conditions and the following disclaimer.
### 2. Redistributions in binary form must reproduce the above copyright notice,
### this list of conditions and the following disclaimer in the documentation
### and/or other materials provided with the distribution.
### 3. Neither the name of the copyright holder nor the
### names of its contributors may be used to endorse or promote products
### derived from this software without specific prior written permission.
###
################################################################################
###
### THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
### AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
### IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
### ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
### LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
### CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
### SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
### INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
### CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
### ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
### POSSIBILITY OF SUCH DAMAGE.
### @endcond
################################################################################

project(libbase ASM)

SET(SOURCE
    crt0.S
    boot-helper.S

    exception.c
    libc.c
    crc16.c
    crc32.c
    console.c
    system.c
    id.c        include/id.h
    uart.c      include/uart.h
    time.c
    qsort.c
    strtod.c
    spiflash.c
    strcasecmp.c
    i2c.c
    div64.c     include/div64.h
    progress.c  include/progress.h
    memtest.c
    vsnprintf.c

    include/inet.h
    include/stdio.h
    include/stdlib.h
    include/endian.h
    include/ctype.h
)

# PPC64LE library
ppc64le_add_library(${PROJECT_NAME} STATIC ${SOURCE})
target_include_directories(${PROJECT_NAME} PUBLIC include)
target_compile_definitions(${PROJECT_NAME} PRIVATE "-DNO_FLINT") # Disable linting
target_compile_definitions(${PROJECT_NAME} PRIVATE "-DSMALL_CRC")

format_target_sources(${PROJECT_NAME})

# No-float variant of the library.
ppc64le_add_library(${PROJECT_NAME}-nofloat STATIC ${SOURCE})
target_include_directories(${PROJECT_NAME}-nofloat PUBLIC include)

target_compile_definitions(${PROJECT_NAME}-nofloat PRIVATE "-DNO_FLOAT")
target_compile_definitions(${PROJECT_NAME}-nofloat PRIVATE "-DSMALL_CRC")
target_compile_definitions(${PROJECT_NAME}-nofloat PRIVATE "-DNO_FLINT") # Disable linting
