ROOT_DIR:=$(abspath ${dir $(lastword $(MAKEFILE_LIST))}/../../)

TRIPLE=powerpc64le-linux-gnu
CPU=microwatt
CPUFLAGS=-m64 -mabi=elfv2 -msoft-float -mno-vsx -mno-altivec -mlittle-endian -mstrict-align -fno-stack-protector -mcmodel=small -D__microwatt__
CPUENDIANNESS=little
CLANG=1

export BUILDINC_DIRECTORY
BUILDINC_DIRECTORY=${ROOT_DIR}/include
LIBBASE_DIRECTORY=${ROOT_DIR}/libbase
LIBLITEDRAM_DIRECTORY=${ROOT_DIR}/liblitedram
LIBLITEETH_DIRECTORY=${ROOT_DIR}/libliteeth
LIBLITESPI_DIRECTORY=${ROOT_DIR}/liblitespi
LIBLITESDCARD_DIRECTORY=${ROOT_DIR}/liblitesdcard
BIOS_DIRECTORY=${ROOT_DIR}/bios
